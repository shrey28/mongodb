const { MongoClient } = require("mongodb");

const url =
  "mongodb+srv://shrey:n3u5lOm1WqN3IqUu@cluster0.hzhcr8v.mongodb.net/?retryWrites=true&w=majority";

//create new client

const client = new MongoClient(url);

const dbConnect = async () => {
  try {
    // #connect to db
    await client.connect();
    console.log("Connction successfully");

    // #create new to db
    const db = client.db("School");

    //create collection
    const Students = db.collection("Students");

    // #create document(student)-single

    // const student = await Students.insertOne({
    //     name:"Ashwary Pathak",
    //     city:"Indore"
    // })

    //  const student = await Students.insertMany([
    //     {name:"Ashwary Pathak",city:"indore"},
    //     {name:"Kartik Neema",city:"khargon"},
    //     {name:"Abhishek Pal",city:"indore"}
    // ])

    // #find all student
    // const allstudent = await Students.find().toArray()
    // console.log(allstudent)

    // #find a student by name Emmanuuel
    // const foundStudent = await Students.findOne({ name: "Ashwary Pathak" });
    // console.log(foundStudent);

    //find all student city name
    // const foundStudent = await Students.find({ city: "khargon" }).toArray();
    // console.log(foundStudent)

    //update record
//     const updatedStudent = await Students.updateOne({city:"khargon"},
//     {
//       $set:{city:'indore'}
//     }
//     )
// console.log(updatedStudent)


//deleted student 

const deletedStudent = await Students.deleteOne({name:"Abhishek Pal"})
console.log(deletedStudent)
  } catch (err) {
    console.log(err);
  }
};

dbConnect();
